package TaxDemo.com.tax.domain.elements;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import com.tax.utilities.Utility;


@SuppressWarnings("unused")
public class TestTaxCalculator {
		   
	private static Utility utility;
	private Province province, province2, province3;
	private Employee employee, employee2, employee3;
		
	@Before
	public void setup() {
			
		utility = new Utility();
		province = new Province("alberta");
		province2 = new Province("yukon");
		province3 = new Province("ontario");
		employee = new Employee("Opeyemi Adesina", 22, 55000.00, province);
		employee2 = new Employee("Atar Sidhu", 25, 50000.00, province3);
		employee3 = new Employee("Rod Neufeld", 75, 80000.00, province2);
	}
		
	@Test
	public void testProvince() {
		
		//tests whether the name is set correctly
		Assert.assertEquals("alberta", province.getName());
		province.setName("manitoba");
		Assert.assertEquals("manitoba", province.getName());
		province.setName("yukon");
		Assert.assertEquals("yukon", province.getName());
		province.setName("ontario");
		Assert.assertEquals("ontario", province.getName());

	}
	
	@Test
	public void testEI() {
		
		EmploymentInsurance ei = new EmploymentInsurance(employee); //856.36
		assertEquals(856.36, ei.getAmount(), 0);
		EmploymentInsurance ei2 = new EmploymentInsurance(employee3); //856.36
		assertEquals(856.36, ei2.getAmount(), 0);
	}
	
	@Test
	public void testProvincialTax() {
		
		ProvincialTax provincialTax = new ProvincialTax(employee); //5500 CAD
		assertEquals(5500, employee.totalDeductions(), 0);
				
		// Student created code testing
		ProvincialTax provincialTax2 = new ProvincialTax(employee2);
		Assert.assertEquals(2740.66, employee2.totalDeductions(), 0);
		ProvincialTax provincialTax3 = new ProvincialTax(employee3);
		Assert.assertEquals(5938.09, employee3.totalDeductions(), 0);

		Assert.assertEquals(6375.435 , provincialTax.computeTaxes(employee.getGrossIncome(), "manitoba"), 0);
		Assert.assertEquals(2740.6600000000003, provincialTax.computeTaxes(employee2.getGrossIncome(), "ontario"), 0);
		Assert.assertEquals(0.0, provincialTax.computeTaxes(employee.getGrossIncome(), "quebec"), 0);
		Assert.assertEquals(0.0, provincialTax.computeTaxes(employee.getGrossIncome(), "saskatchewan"), 0);
		Assert.assertEquals(0.0, provincialTax.computeTaxes(employee.getGrossIncome(), "newfoundland"), 0);
		Assert.assertEquals(0.0, provincialTax.computeTaxes(employee.getGrossIncome(), ""), 0);
		Assert.assertEquals(5938.09, provincialTax.computeTaxes(employee3.getGrossIncome(), "yukon"), 0);
		
		//Testing different gross incomes for provincial taxes
		Assert.assertEquals(14175.6, provincialTax.computeTaxes(140000.00, "alberta"), 0);
		Assert.assertEquals(16600.96, provincialTax.computeTaxes(160000.00, "alberta"), 0);
		Assert.assertEquals(320552.16, provincialTax.computeTaxes(2200000.00, "alberta"), 0);
		Assert.assertEquals(50552.16, provincialTax.computeTaxes(400000.00, "alberta"), 0);
		Assert.assertEquals(640.0, provincialTax.computeTaxes(10000.00, "yukon"), 0);
		Assert.assertEquals(7793.7789999999995, provincialTax.computeTaxes(100000.00, "yukon"), 0);
		Assert.assertEquals(19634.792, provincialTax.computeTaxes(200000.00, "yukon"), 0);
		Assert.assertEquals(3240.00, provincialTax.computeTaxes(30000, "manitoba"), 0);
		Assert.assertEquals(9999.570000000002, provincialTax.computeTaxes(80000, "manitoba"), 0);		
		Assert.assertEquals(22935.0718, provincialTax.computeTaxes(230000, "ontario"), 0);
		Assert.assertEquals(6411.071800000001, provincialTax.computeTaxes(90000, "ontario"), 0);
		Assert.assertEquals(14323.071800000001, provincialTax.computeTaxes(160000, "ontario"), 0);
		Assert.assertEquals(252.50000000000003, provincialTax.computeTaxes(5000, "ontario"), 0);
		
	}
	
	@Test
	public void testFederalTax() {
		
		FederalTax federalTax = new FederalTax(employee); //8605.57
		assertEquals(8605.57, employee.totalDeductions(), 0);
		
		// Student created code for testing
		FederalTax federalTax2 = new FederalTax(employee2);
		Assert.assertEquals(7580.57, employee2.totalDeductions(), 0);

		// Student created code for testing
		FederalTax federalTax3 = new FederalTax(employee3);
		Assert.assertEquals(13730.57, employee3.totalDeductions(), 0);

	}
	
	@Test
	public void testCPP() {
		
		CanadianPensionPlan cpp = new CanadianPensionPlan(employee); //2887.50
		assertEquals(2887.50, employee.totalDeductions(), 0);
		
	}
	
	
	@Test
	public void testEmployee() {
		

		EmploymentInsurance ei = new EmploymentInsurance(employee);
		ProvincialTax provincialTax = new ProvincialTax(employee);
		FederalTax federalTax = new FederalTax(employee);
		CanadianPensionPlan cpp = new CanadianPensionPlan(employee);

		assertEquals(4, employee.getDeductions().size());
		assertEquals(17849.43, employee.totalDeductions(), 0);
		
		// Student created code for testing
		Assert.assertEquals("Opeyemi Adesina", employee.getName());
		employee.setName("Atar Sidhu");
		
		Assert.assertEquals(22, employee.getAge(), 0);
		employee.setAge(25);
		
		Assert.assertTrue(employee.setGrossIncome(employee.getGrossIncome()));
	
		Assert.assertEquals(4, employee.numberOfDeductions());
		Assert.assertTrue(employee.hasDeductions());
		
		Assert.assertEquals(0, Employee.minimumNumberOfDeductions());
		
		Assert.assertEquals(37150.57, employee.netIncome(), 0);
				
	}
	
	// Student created test method
	@Test
	public void testDeduction() {
		Deduction ddc = new Deduction(employee);
		CanadianPensionPlan aNewCanadianPensionPlan = new CanadianPensionPlan(employee);
		EmploymentInsurance aNewEmploymentInsurance = new EmploymentInsurance(employee);
		Assert.assertFalse(ddc.hasTaxs());
		ProvincialTax albertaTax = new ProvincialTax(employee);
		FederalTax canadianTax = new FederalTax(employee);
		
		Assert.assertEquals(0, ddc.numberOfTaxs());
		Assert.assertEquals(0, Deduction.minimumNumberOfTaxs());
		Assert.assertEquals(2, Deduction.maximumNumberOfTaxs());
		
		Assert.assertFalse(ddc.hasCanadianPensionPlan());
		Assert.assertFalse(ddc.hasEmploymentInsurance());
		Assert.assertTrue(ddc.setCanadianPensionPlan(aNewCanadianPensionPlan));
		Assert.assertTrue(ddc.setEmploymentInsurance(aNewEmploymentInsurance));
		Assert.assertTrue(ddc.addTax(albertaTax));
		Assert.assertFalse(ddc.addTax(albertaTax));
		Assert.assertTrue(ddc.removeTax(albertaTax));
		Assert.assertFalse(ddc.removeTax(albertaTax));
		
		Assert.assertTrue(ddc.setTaxs(albertaTax, canadianTax));
		Assert.assertFalse(ddc.setTaxs(albertaTax, canadianTax, albertaTax));
		
		ddc.removeTax(albertaTax);
		Assert.assertTrue(ddc.addTaxAt(albertaTax, ddc.indexOfTax(canadianTax)));
		Assert.assertFalse(ddc.addTaxAt(albertaTax, ddc.indexOfTax(canadianTax)));
		ddc.removeTax(canadianTax);;
		Assert.assertTrue(ddc.addOrMoveTaxAt(canadianTax, ddc.indexOfTax(albertaTax)));
		Assert.assertTrue(ddc.addOrMoveTaxAt(canadianTax, ddc.indexOfTax(albertaTax)));
		
		Assert.assertNotEquals(null, "", ddc.toString());
		
		Assert.assertEquals(2,  ddc.numberOfTaxs());
		
		//The deductions should now have taxes
		Assert.assertTrue(ddc.hasTaxs());
		
		//Return the values set earlier
		Assert.assertTrue(ddc.hasCanadianPensionPlan());
		Assert.assertTrue(ddc.hasEmploymentInsurance());
		Assert.assertEquals(aNewCanadianPensionPlan, ddc.getCanadianPensionPlan());
		Assert.assertEquals(aNewEmploymentInsurance, ddc.getEmploymentInsurance());
		List<Tax> checkTaxs = ddc.getTaxs();
		Assert.assertEquals(checkTaxs.get(0), ddc.getTax(0));
		Assert.assertEquals(checkTaxs.get(1), ddc.getTax(1));
		Assert.assertEquals(albertaTax, ddc.getTax(ddc.indexOfTax(albertaTax)));
		Assert.assertEquals(canadianTax, ddc.getTax(ddc.indexOfTax(canadianTax)));
		
		
		
		ddc.setEmployee(null);
		ddc.setEmployee(employee2);
		
	}
		
	@After
	public void teardown() {
		this.employee.delete();
		this.province.delete();
		this.employee3.delete();
		this.province2.delete();
	}
}
