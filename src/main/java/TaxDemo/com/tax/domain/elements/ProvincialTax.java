package TaxDemo.com.tax.domain.elements;

//line 138 "../../../../../../../ump/tmpjntyadj0/model.ump"
//line 334 "../../../../../../../ump/tmpjntyadj0/model.ump"
public class ProvincialTax extends Tax
{

//------------------------
// MEMBER VARIABLES
//------------------------

//------------------------
// CONSTRUCTOR
//------------------------

public ProvincialTax(Employee aEmployee)
{
 super(aEmployee);
 // line 145 "../../../../../../../ump/tmpjntyadj0/model.ump"
 setAmount(computeTaxes( grossIncome(), getProvince() ));
 // END OF UMPLE AFTER INJECTION
}

//------------------------
// INTERFACE
//------------------------

public void delete()
{
 super.delete();
}


/**
* 
* operation computes provincial taxes for the the province of Alberta
* @param grossIncome employee's gross income prior to any tax deduction
* @return provincialTax deductible provincial tax
*/
// line 155 "../../../../../../../ump/tmpjntyadj0/model.ump"
private double alberta(double grossIncome){
 //setting up tax percentages and their respective lower bounds
 double [] taxPercentages = {0.1, 0.12, 0.13, 0.14};
	double [] lowerBounds = {131220.0, 157464.0, 209952.0, 314928.0};
 
 //provincialTaxBracket = "10.0% [$0 .. $131,220.01)
 if( grossIncome >= 0.0 && grossIncome < 131220.01) { 
   return 0.1 * grossIncome;
 }
 
 //provincialTaxBracket = "12.0% [$131,220.01 .. $157,464.01)
 else if( grossIncome >= 131220.01 && grossIncome < 157464.01) {
   return ( grossIncome - 131220.0 ) * .12 + computeTaxCategoryPay( taxPercentages, lowerBounds, 0 );
 }
  
 //provincialTaxBracket = "13.0% [$157,464.01 .. $209,952.01)
 else if( grossIncome >= 157464.01 && grossIncome < 209952.01) {
   return ( grossIncome - 157464.0 ) * .13 + computeTaxCategoryPay( taxPercentages, lowerBounds, 1 );
 }
 
 //provincialTaxBracket = "14.0% [$209,952.01 .. $314,928.00)
 else if( grossIncome > 209952.01 && grossIncome < 314928.00 ) {
    return ( grossIncome - 209952.0 ) * .14 + computeTaxCategoryPay( taxPercentages, lowerBounds, 2 );
 }
 
 //provincialTaxBracket = "15.0% [$314,928.01 .. )
 else {
    return ( grossIncome - 314928.0 ) * .15 + computeTaxCategoryPay( taxPercentages, lowerBounds, 3 );
 }
}

/**
 *
 * operation computes provincial taxes for the the provincial territory of the Yukon
 * @param grossIncome employee's gross income prior to any tax deduction
 * @return provincialTax deductible provincial tax
 * @author Rod Neufeld
 */
private double yukon(double grossIncome){
    //setting up tax percentages and their respective lower bounds
    double [] taxPercentages = {0.064, 0.09, 0.109, 0.128, 0.15};
    double [] lowerBounds = {48535.0, 97069.0, 150473.0, 500000.0};

    //provincialTaxBracket = "6.4% [$0 .. $48535.0)
    if( grossIncome >= 0.0 && grossIncome < 48535.0) {
        return 0.064 * grossIncome;
    }

    //provincialTaxBracket = "9.0% [$48535.01 .. $97069.00)
    else if( grossIncome >= 48535.01 && grossIncome < 97069.01) {
        return ( grossIncome - 48535.0 ) * .09 + computeTaxCategoryPay( taxPercentages, lowerBounds, 0 );
    }

    //provincialTaxBracket = "10.9% [$97069.01 .. $150473.0)
    else if( grossIncome >= 97069.01 && grossIncome < 150473.01) {
        return ( grossIncome - 97069.00 ) * .109 + computeTaxCategoryPay( taxPercentages, lowerBounds, 1 );
    }

    //provincialTaxBracket = "12.8% [$209,952.01 .. $314,928.00)
    else if( grossIncome >= 150473.01 && grossIncome < 500000.01 ) {
        return ( grossIncome - 150473.0 ) * .128 + computeTaxCategoryPay( taxPercentages, lowerBounds, 2 );
    }

    //provincialTaxBracket = "15.0% [$500000.01  .. )
    else {
        return ( grossIncome - 500000.0 ) * .15 + computeTaxCategoryPay( taxPercentages, lowerBounds, 3 );
    }


}

/**
*
* operation computes provincial taxes for the the province of Monitoba
* @param grossIncome employee's gross income prior to any tax deduction
* @return provincialTax deductible provincial tax
* @author Andrew Ledger
*/
private double manitoba(double grossIncome){
   //setting up tax percentages and their respective lower bounds
   double [] taxPercentages = {0.108, 0.1275, 0.0174};
   double [] lowerBounds = {32670.0, 70610.0};

   //provincialTaxBracket = "10.8% [$0 .. $32670.0)
   if( grossIncome >= 0.0 && grossIncome < 32670.0) {
       return 0.108 * grossIncome;
   }

   //provincialTaxBracket = "12.75% [$32670.01 .. $70610.00)
   else if( grossIncome >= 32670.01 && grossIncome < 70610.01) {
       return ( grossIncome - 32670.0 ) * .1275 + computeTaxCategoryPay( taxPercentages, lowerBounds, 0 );
   }

   //provincialTaxBracket = "17.4% [$70610.0  .. )
   else {
       return ( grossIncome - 70610.0 ) * .174 + computeTaxCategoryPay( taxPercentages, lowerBounds, 1 );
   }
}


/**
* 
* operation computes provincial taxes for the the province of ontario
* @param grossIncome employee's gross income prior to any tax deduction
* @return provincialTax deductible provincial tax
* @author Atar Sidhu
*/
private double ontario(double grossIncome){
 //setting up tax percentages and their respective lower bounds
 double [] taxPercentages = {0.0505, 0.0915, 0.1116, 0.1216};
	double [] lowerBounds = {44740.0, 89482.0, 150000.0, 220000.0};
 
 //provincialTaxBracket = "5.05% [$0 .. $44,740.01)
 if( grossIncome >= 0.0 && grossIncome < 44740.01) { 
   return 0.0505 * grossIncome;
 }
 
 //provincialTaxBracket = "9.15% [$44,740.01 .. $89,482.01)
 else if( grossIncome >= 44740.01 && grossIncome < 89482.01) {
   return ( grossIncome - 44740.0 ) * .0915 + computeTaxCategoryPay( taxPercentages, lowerBounds, 0 );
 }
  
 //provincialTaxBracket = "11.16% [$89,482.01 .. $150,000.01)
 else if( grossIncome >= 89482.01 && grossIncome < 150000.01) {
   return ( grossIncome - 89482.0 ) * .1116 + computeTaxCategoryPay( taxPercentages, lowerBounds, 1 );
 }
 
 //provincialTaxBracket = "12.16% [$150,000.01 .. $220,000.00)
 else if( grossIncome > 150000.01 && grossIncome < 220000.00 ) {
    return ( grossIncome - 150000.0 ) * .1216 + computeTaxCategoryPay( taxPercentages, lowerBounds, 2 );
 }
 
 //provincialTaxBracket = "13.16% [$220,000.01 .. )
 else {
    return ( grossIncome - 220000.0 ) * .1316 + computeTaxCategoryPay( taxPercentages, lowerBounds, 3 );
 }
}




/**
* operation computes provincial taxes for the the province of alberta
* @param grossIncome employee's gross income prior to any tax deduction
* @param provinceName employee's work province
* @return provincialTax deductible provincial tax
*/
// line 192 "../../../../../../../ump/tmpjntyadj0/model.ump"
public double computeTaxes(double grossIncome, String provinceName){
 //province's name is converted to all lower case letters 
 String name = provinceName.toLowerCase();
 switch( name ) {
   
   case "alberta" : 
     return alberta( grossIncome );
   case "manitoba" : 
	   return manitoba( grossIncome );
   case "ontario" : 
	   return ontario ( grossIncome );
   case "quebec" : return 0.0;
   case "saskatchewan" : return 0.0;
   case "newfoundland" : return 0.0;
   case "yukon" :
    return yukon( grossIncome );
   default: return 0.0;
 }
}

}